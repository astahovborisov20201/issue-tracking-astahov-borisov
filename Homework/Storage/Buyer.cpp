#include "Buyer.h"
#include "Header.h"

Buyer::Buyer() // ������ �����������
{
	id = 0;
	name = "";
	vector<Warehouse> empty;
	list = empty;
}

Buyer::Buyer(int id, string name) // �����������, ������� ����� ������������
{
	this->id = id;
	this->name = name;
}

void Buyer::printInfo() //�����
{

	cout << "ID: " << id << setw(4) << " | Name: " << name << endl;
}

void Buyer::printList(vector<Warehouse> list)
{
	for (int i=0;i<list.size();i++)
	{
		string _name = list[i].getname();
		int _id = list[i].getId();
		int _price = list[i].getprice();
		cout<<"Product "<<_id<<"|Name "<<_name<<"|Price "<<_price; 
	}
}

string Buyer::getName() // �������� ���
{
	return name;
}

int Buyer::getId() // �������� id
{
	return id;
}

void Buyer::AddList(Warehouse product)//���������� ������ � ������
{
	list.push_back(product);
}

void Buyer::DeleteProduct(int flag) //�������� ������ �� ������ �������
{
	vector<Warehouse> temp;
	for (int i = 0; i < list.size(); i++)
	{
		if (i != flag)
		{
			temp.push_back(list[i]);
		}
	}
	list = temp;
}