#include "Warehouse.h"
#include "Header.h"

Warehouse::Warehouse() // �����������
{
	id = 0;
	name = "";
	price = id;
}

Warehouse::Warehouse(int id, string name, int price) // ��� ���� �����������
{													 // ������������� ��� ���������
	this->id = id;
	this->name = name;
	this->price = price;
}

void Warehouse::printInfo() // ����� ����������
{
	cout << "ID: " << id << setw(8) << " | name: " << name << setw(8) << " | price: " << price << endl;
}

string Warehouse::getname() // ��������� �������� ���������
{
	return name;
}

int Warehouse::getprice() // ��������� ����� ���������
{
	return price;
}

void Warehouse::setprice(int _price) // ���������� ��� ���������
{
	price = _price;
}

int Warehouse::getId() // �������� �������������
{
	return id;
}