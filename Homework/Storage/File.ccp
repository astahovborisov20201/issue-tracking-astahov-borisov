#include "File.h"
#include "Header.h"


File::File()					// �����������
{
	ifstream file;				// ������ �����
	file.open(wPath, ios::app); // ������� ���� (���� �� ������) 
	file.close();				// ��������� ����
	file.open(bPath, ios::app); // ������� ���� (���� �� ������) 
	file.close();
}

void File::saveBuyersData(vector<Buyer> Buyers) // ���������� �����������
{
	ofstream file(wPath);
	for (int i=0;i<Buyers.size();i++)
	{											//��������� - �������� ������ �������
		file << Buyers[i].getId()<<" "<< Buyers[i].getName() <<endl;
		for (int j = 0; j < Buyers[i].list.size(); j++)
		{
			file << Buyers[i].list[i].getId() << " " << Buyers[i].list[i].getname() << " " << Buyers[i].list[i].getprice() << endl;
		}
	}
	file.close();								// �������� ����
}

void File::saveWarehouseData(vector<Warehouse> Warehouse) // ���������� ������ �������
{
	ofstream file(bPath);
	for (auto Warehouse : Warehouse)
	{
		file << Warehouse.getId() << " " << Warehouse.getname() << " " << Warehouse.getprice() << endl;
	}
	file.close();
}

vector<Buyer> File::getBuyersList() // ���������� ������� ����
{
	vector<Buyer> result;
	ifstream file(wPath);			//���������
	while (!file.eof())
	{
		string prefix, name;
		int id;
		file >> id >> name;
		if (name.length())
		{
			Buyer that(id, name);
			result.push_back(that);
		}
	}
	return result;
}


vector<Warehouse> File::getWarehouse()			// �������� �����������
{
	vector<Warehouse> result;
	ifstream file(bPath);
	while (!file.eof())
	{
		string post;
		int lister;
		int id;
		file >> id >> post >> lister;			// ������
		if (post.length())						// ���� �������� ��������� ����������
		{
			Warehouse that(id, post, lister);   // ������� ������ ������ Warehouse � ������� �����������
			result.push_back(that);				// ��������� � ������
		}
	}
	return result;								// ���������� ������
}