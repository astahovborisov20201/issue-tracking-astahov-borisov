#include "Header.h"

using namespace std;

void DrawMenu(vector<string> menu)
{
	int list = 1;
	for (auto item : menu)
	{
		cout << list << ". " << item << endl;
		list++;
	}
}