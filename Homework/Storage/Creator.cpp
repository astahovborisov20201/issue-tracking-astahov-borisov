#include "Header.h"

using namespace std;

Warehouse AddProduct(int lastId)
{
	string name;
	cout << "Enter name of new product: ";
	cin >> name;
	int price;
	cout << "Enter price: ";
	cin >> price;
	Warehouse v(lastId++, name, price);
	cout << "Fin." << endl << endl;
	system("pause");
	return v;
}

Buyer AddBuyer(int lastId)
{
	string name;
	cout << "Enter name of new buyer: ";
	cin >> name;
	Buyer h(lastId++, name);
	cout << "Fin." << endl << endl;
	system("pause");
	return h;
}