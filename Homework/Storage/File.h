#pragma once
#include <string>
#include "Buyer.h"
#include <vector>
#include "Warehouse.h"

using namespace std;

class File
{
private:
	string wPath = "Buyers.txt"; 
	string bPath = "Warehouse.txt";
public:
	File(); // �����������
	vector<Warehouse> getWarehouse(); // ��������� ������ �������
	vector<Buyer> getBuyersList(); // ���������� ���� �����
	void saveBuyersData(vector<Buyer>); // ��������� �����
	void saveWarehouseData(vector<Warehouse>); // ��������� ����� �������
};