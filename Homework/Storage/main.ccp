#include "Header.h"

using namespace std;

int main()
{
	setlocale(LC_ALL, ""); 
	vector <Warehouse> Warehouse;														// ������ ������ ��� ������ �������
	vector <Buyer> Buyers;																// ������ ������ ��� �����
	File file;																			// ������ ������ ����
	cout << "Retrieving information from a database of Warehouse..." << endl;
	Warehouse = file.getWarehouse();													// �������� ����� �������
	cout << "Count of Product positions: " << Warehouse.size() << endl << endl;
	cout << "Retrieving information from a database of orders..." << endl;
	Buyers = file.getBuyersList();														// �������� �����
	cout << "Count of Orders: " << Buyers.size() << endl << endl;
	vector<string> menu = { "List of Products", "List of Orders/Buyers List", "Add product", "Add order/buyer", "Manage order/Buyers", "Save data" };
	int flag;
	do
	{
		system("cls");
		DrawMenu(menu);
		cout << endl << "Enter item number or 0 to exit: ";
		cin >> flag;
		switch (flag)
		{
			case 1:
			{
				PrintProductList(Warehouse);											//����� ����
				system("pause");
				break;
			}
			case 2:
			{
				PrintBuyers(Buyers);
				system("pause");
				break;
			}
			case 3:
				Warehouse.push_back(AddProduct(Warehouse.size()));
				break;
			case 4:
				Buyers.push_back(AddBuyer(Buyers.size()));
				break;
			case 5:
				ManageOrder(Buyers, Warehouse);
				break;
			case 6:
			{
				file.saveBuyersData(Buyers);
				file.saveWarehouseData(Warehouse);
				system("pause");
				break;
			}
			system("cls");
		}
	} while (flag != 0);
	return 0;
}