#include "Header.h"

using namespace std;

void PrintProductList(vector<Warehouse> Warehouse)
{
	cout << "=== [Products] ===" << endl;
	for (auto v : Warehouse)
	{
		v.printInfo();
	}
	cout << endl;
}
void PrintBuyers(vector<Buyer> Buyers)
{
	cout << "=== [Buyers] ===" << endl;
	for (auto h : Buyers)
	{
		h.printInfo();
	}
	cout << endl;
}