#pragma once
#include "File.h"
#include "Buyer.h"
#include "Warehouse.h"
#include <string>
#include <iostream>
#include <iomanip>
#include <vector>
#include <fstream>

void DrawMenu(vector<string>);
void PrintProductList(vector<Warehouse>);
void PrintBuyers(vector<Buyer>);
Warehouse AddProduct(int);
Buyer AddBuyer(int);
void ManageOrder(vector<Buyer>&, vector<Warehouse>&);
